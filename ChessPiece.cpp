/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#include "ChessPiece.h"
#include <cmath>

int Pawn::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const{
  unsigned int distance = ((start.x - end.x) * (start.x - end.x) + (start.y - end.y) * (start.y - end.y));
  int movement = end.y - start.y;
  
  //Can't move backwards, can't move sideways, can't move more than 2 spaces away
  if((_owner == 0 && movement <= 0) || (_owner == 1 && movement >= 0) || distance > 4){
    return MOVE_ERROR_ILLEGAL;
  }
 
  //Check to see if pawn is at starting position for double jump
  if(distance == 4){
    if(_owner == 0 && !(start.y == 1)){
      return MOVE_ERROR_ILLEGAL;
    }
    if(_owner == 1 && !(start.y == 6)){
      return MOVE_ERROR_ILLEGAL;
    }
    int new_y = (start.y + end.y) / 2;
    trajectory.push_back(Position(start.x, new_y));
    return 3; // pawn special movement
  }

  //Must be diaganol capture
  if(distance == 2){
    return 2; // pawn special movement 
  }
  
  //Movement must be one step forward
  return 4; //pawn special movement
}

int Rook::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const{
  if (end.x - start.x != 0 && end.y - start.y == 0){ //Horizontal Movement
    signed int change_x = (signed int) end.x -  (signed int) start.x;
    if (change_x > 0){
      for(unsigned int i = start.x + 1; i < end.x; i++){ //Movement to the right
	trajectory.push_back(Position(i, start.y));
      }
    } else {
      for(unsigned int i = start.x - 1; i > end.x; i--){ //Movement to the left
	trajectory.push_back(Position(i, start.y));
      }
    }
    return SUCCESS;
  }
	
  if (end.x - start.x == 0 && end.y - start.y != 0) {//Vertical Movement
    signed int change_y = (signed int) end.y - (signed int) start.y;
    if (change_y> 0){
      for(unsigned int i = start.y + 1; i < end.y; i++){ //Movement down 
	trajectory.push_back(Position(start.x, i));
      }
      
    } else {
      for(unsigned int i = start.y - 1; i > end.y; i--){ //Movement up
	trajectory.push_back(Position(start.x, i));
      }
    }
    return SUCCESS;
  }
  return MOVE_ERROR_ILLEGAL;
}

int Knight::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
   unsigned int distance = ((start.x - end.x) * (start.x - end.x) + (start.y - end.y) * (start.y - end.y));
   trajectory.push_back(start); //Fixes unsused parameter error
   trajectory.erase(trajectory.begin());
   if(distance == 5){ //Gives L-shape
     return SUCCESS;
   } else {
     return MOVE_ERROR_ILLEGAL;
   }
}

int Bishop::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const { 
  //movement in x-direction
  int delta_x = (end.x - start.x);
  //movement in y-direction
  int delta_y = (end.y - start.y);
  
  //absolute value of movement in x-direction, also the unsigned diagonal distance     
  //int abs_delta_x = abs(delta_x);
  //absolute value of movement in y-direction
  //int abs_delta_y = abs(delta_y);
  int output;
  
  // If the attempted motion is in straight line, return an error
  if(delta_x == 0 || delta_y == 0){
    return MOVE_ERROR_ILLEGAL;
  }
  else{
    output = Piece::valid_diagonal(start, delta_x, delta_y, trajectory); //callson Piece class function
    if(output < 0){
      return MOVE_ERROR_ILLEGAL;
    }
  }  
  return SUCCESS;
}
	
       
int Queen::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
  //movement in x-direction
  int delta_x = (end.x - start.x);
  //movement in y-direction
  int delta_y = (end.y - start.y);
  //absolute value of movement in x-direction	
  int abs_delta_x = abs(delta_x);
  //absolute value of movement in y-direction
  int abs_delta_y = abs(delta_y);
  
  int output;
  int flag1, flag2;

  if(delta_x == 0 && delta_y == 0){
    return MOVE_ERROR_ILLEGAL;
  }
  
  // If the motion is not in straight line, check to see if it is a valid
  // diagonal move
  if(delta_x != 0 && delta_y != 0){
    output = Piece::valid_diagonal(start, delta_x, delta_y, trajectory);
    if(output < 0){
      return MOVE_ERROR_ILLEGAL;
    }
  }
  
  else{
    // Now that the movement is confirmed to have to be in a straight line
    // we know that either delta_x or delta_y will be zero so we can just add both
    // to get the straight line distant since we dont know which is zero
    int distance = abs_delta_x + abs_delta_y;
    
    //Need to add all spaces between initial and final locations. We can do this through delta_x and delta_y
    
    //Define flag1 and flag2 on the following properties: here if delta_x is zero, then we will only add steps 
    //along the trajectory that are in the y-direction. Furthermore, we want to be sure to add the steps along
    //the correct positive or negative y-direction based on whether the ending point of the piece is above or below
    //the starting point (i.e. wether the ending point is < or > the starting point). We follow the same logic for
    //the x-direction. If delta-y is zero, then we will only add steps along the trajectory that are in the x-direction.
    //Again, we want to be sure to add steps only to the right or the left of the starting point based on whether
    //the ending point of x is < or > the starting point. These flags are later used in the loop below.
    if (delta_x == 0){
      flag1 = 1;} //delta_x is zero
    else{	
      flag1 = 2;} //delta_y is zero
    
    if(flag1 == 1){ //delta_y is nonzero, i.e it is moving only in y-direction
      if(end.y < start.y){
	flag2 = 1;} //it is moving down the board (-y direction)
      else{
	flag2 = 2;} //it is moving up the board (+y direction)
    }
    else{ //delta_x is nonzero, i.e. it is moving only in the x-direction
      if(end.x < start.x){
	flag2 = 1;} //it is moving to the left of the board
      else{
	flag2 = 2;} //it is moving to the right of the board
    }
    
    
    //Define a temporary position which eventually hold all the steps between start and end
    Position step;
    
    //Flags are used here...
    for(int i = 1; i < distance; i++){
      if(flag1 == 1){
	if (flag2 == 1){
	  step = Position(start.x,start.y - i);}
	else if (flag2 == 2){
	  step = Position(start.x, start.y + i);}
      }
      else if (flag1 == 2){
	if(flag2 == 1){
	  step = Position(start.x - i,start.y);}
	else if (flag2 == 2){
	  step = Position(start.x + i, start.y);}
      }
      trajectory.push_back(step);
    }
  }
  return SUCCESS;
}

int King::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
  unsigned int distance = ((start.x - end.x) * (start.x - end.x) + (start.y - end.y) * (start.y - end.y));
  trajectory.push_back(start);
  trajectory.erase(trajectory.begin());

  if(distance != 2 && distance != 1){// Don't want the king to move far
    return MOVE_ERROR_ILLEGAL;
  }
  return SUCCESS;
}

int Ghost::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
  trajectory.push_back(start);
  trajectory.push_back(end);
  trajectory.erase(trajectory.begin());
  trajectory.erase(trajectory.begin());
  //Fixes unsused parameter error, not ever called
  
  return SUCCESS;
}
