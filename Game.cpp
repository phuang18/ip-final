/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#include <iostream>
#include <cassert>
#include <cctype>
#include <string>
#include <sstream>
#include <fstream>
#include "Game.h"
#include "Prompts.h"
#include "Piece.h"
#include "Terminal.h"

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::ofstream;

Game::~Game() {
    // Delete the factories used to generate pieces
    for (size_t i = 0; i < _registered_factories.size(); i++) {
        delete _registered_factories[i];
    }
    // Delete any other dynamically-allocated resources here
    for(std::vector<Piece*>::iterator itt = _pieces.begin(); itt != _pieces.end(); itt++){
      delete (*itt);
    }
}

// Create a Piece on the board using the appropriate factory.
// Returns true if the piece was successfully placed on the board.
bool Game::init_piece(int piece_type, Player owner, Position pos) {
    Piece* piece = new_piece(piece_type, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!valid_position(pos)) {
        Prompts::out_of_bounds();
        return false;
    }

    // Fail if the position is occupied
    if (get_piece(pos)) {
        Prompts::blocked();
        return false;
    }
    _pieces[index(pos)] = piece;
    return true;
}

// Get the Piece at a specified Position.  Returns nullptr if no
// Piece at that Position or if Position is out of bounds.
Piece* Game::get_piece(Position pos) const {
    if (valid_position(pos))
        return _pieces[index(pos)];
    else {
        Prompts::out_of_bounds();
        return nullptr;
    }
}

// Perform a move from the start Position to the end Position
// The method returns an integer status where a value
// >= 0 indicates SUCCESS, and a < 0 indicates failure
void Game::make_move(Position start, Position end, int move_check) {  
  switch(move_check){
  case -6:
    Prompts::no_piece();
    break;
    
  case -5:
    Prompts::blocked();
    break;

  case -3:
    Prompts::must_handle_check();
    break;

  case -2:
    Prompts::cannot_expose_check();
    break;
 
  case -1:
    Prompts::illegal_move();
    break;
  
  case 1:
    swap_positions(start, end);
    break;
    
  case 3:
    capture(start, end);
    Prompts::capture(player_turn());
    break;
  }
}

//Carries out post-turn activities
//Checks for check, stalemate and checkmate
//1 if end game, 0 if not
int Game::end_turn(){
  if(in_check(player_turn())){
    _turn += 1;
    if(checkmate(player_turn()) == 0){
      _turn -= 1;
      Prompts::checkmate(player_turn());
      Prompts::win(player_turn(), _turn);
      return 1;
    } else {
      _turn -= 1;
      Prompts::check(player_turn());
    }
  } else {
    _turn += 1;
    if(checkmate(player_turn()) == 0){
      _turn -= 1;
      Prompts::stalemate();
      return 1;
    }
    _turn -= 1;
  }
  return 0;
}

// Execute the main gameplay loop.
void Game::run(){
  int terminate = 0;
  char input[256];
  string flush;
  int print_count = 0;
  cin.getline(input, 256);
  
  Prompts::player_prompt(player_turn(), turn());
  cin.getline(input, 256);
  string input_string = input;
  
  while(input[0] != 'q' && terminate != 1){   
    coordinates start_end;
    coordinates check_trajectory;

    if(input_string.compare("save") == 0){
      string filename;
      Prompts::save_game();
      cin >> filename;
      save_file(filename);
      cin.getline(input, 256);
    }
  
    else if(input_string.compare("board") == 0){
      print_count = (print_count + 1) % 2;
    }

    else if(input_string.compare("forfeit") == 0){
      _turn += 1;
      Prompts::win(player_turn(), _turn - 1);
      terminate = 1;
    }
    else if(input_string.compare("color_fg") == 0 || input_string.compare("color_all") == 0 || input_string.compare("color_bg") == 0 || input_string.compare("set_default") == 0) {
      color_changer(input_string);
      getline(cin, flush);

    }
    else if(accept_input(start_end, input) == 1){
      Position start = *(start_end.begin());
      Position end = start_end.back();
      
      //Test the given move, but don't actually execute it
      int move_check = move(start, end);      
      
      //Attempts to make move, and outputs message if there is a failure
      make_move(start, end, move_check);
      
      //Checks to see if there is a pawn at the end of the board to convert to a queen
      pawn_into_queen();
      
      //Carries out end of turn functions, changing terminate to end run loop if required
      terminate = end_turn();
      
      //Increments turn if a move was acutally made
      if(move_check > 0 && terminate == 0){
	spooky_turn();      //Allows ghost to move
	_turn += 1;
      }
      
    } //Closes out turn movement

    //Print board if print counter is "on"
    if(print_count == 1){
      print_board();
    }

    //If the game is continuing for another turn, set everything to accept input
    if(terminate == 0){
      Prompts::player_prompt(player_turn(), turn());
      cin.getline(input, 256);
      input_string = input;
    }
  
  }//End of input while loop
  Prompts::game_over();
}

//Attempt to perform the movement of a piece
int Game::move(Position start, Position end){
  coordinates trajectory;
  Piece *star = get_piece(start);
  
  if(star == nullptr){
    return MOVE_ERROR_NO_PIECE;
  } else {
    if(star->owner() != player_turn()){
      return MOVE_ERROR_NO_PIECE;
    } else {
      if((*star).valid_move_shape(start, end, trajectory) > -1 && blockage(trajectory) == 1){
	coordinates check_trajectory;

	//Are we currently in check (required to print correct message after move)
	_turn += 1;
	int already_in_check = in_check(player_turn());
	_turn -= 1;

	//Checks to make sure specific conditions are met for pawn movement
	int specific_to_pawn = (*star).valid_move_shape(start, end, trajectory);
	if(specific_to_pawn == 2 && final_space_check(end, (*star).owner()) != 3){
	  return MOVE_ERROR_ILLEGAL;
	}
	
        if((specific_to_pawn == 3 || specific_to_pawn == 4) && final_space_check(end, (*star).owner()) != 1){
	  return MOVE_ERROR_BLOCKED;
	}

	//Checks the final space and returns information
	switch(final_space_check(end, (*star).owner())){
	
	case -5:
	  return MOVE_ERROR_BLOCKED;

	//Temporarily swap places and see if we place oursevles in check (either again or in new way)
	  //Make sure to undo either way
	case 1:
	  swap_positions(start, end);
	  if(myself_in_check(already_in_check) < 1){
	    int temp = myself_in_check(already_in_check);
	    swap_positions(start, end);
	    return temp;
	  }
	  swap_positions(start, end);
	  return SUCCESS;

	//Temporarily capture a piece and see if we place ourselves in check (either again or in new way)
	  //Make sure to undo either way
	case 3:
	  Piece *captured = get_piece(end);
	  int type = captured->piece_type();
	  Player owner = captured->owner();
	  capture(start, end);
	  if(myself_in_check(already_in_check) < 1){
	    int temp = myself_in_check(already_in_check);
	     init_piece(type, owner, start);
	     swap_positions(start, end);
	     return temp; 
	  }
	  init_piece(type, owner, start);
	  swap_positions(start, end);
	  return MOVE_CAPTURE;
	}
      } else {

	//Error with movement, either blocked or invalid shape
	if(blockage(trajectory) != 1){
	  return MOVE_ERROR_BLOCKED;
	} else {
	  return MOVE_ERROR_ILLEGAL;
	}

      }
    }
  }
  return MOVE_ERROR_ILLEGAL;
}


//Return 0 if you are placing yourself into check, otherwise 1
int Game::myself_in_check(int already_in_check){
  _turn += 1;
  if(in_check(player_turn()) == 1){ //We are in check
    if(already_in_check == 0){ //Were we already in check?
      _turn -= 1;
      return MOVE_ERROR_CANT_EXPOSE_CHECK;
    } else {
      _turn -= 1;
      return MOVE_ERROR_MUST_HANDLE_CHECK;
    }
  }
  _turn -= 1;
  return SUCCESS;
}

//Function to see if a player is currently in check
//Checks if CURRENT player is placing OTHER player in check
int Game::in_check(int player){
  
  //Find the location of their king
  //Fill a Position vector with where enemy pieces are
  Position king = Position(0,0);
  coordinates enemy_pieces;
  for(signed int i = 7; i > -1; i--){
    for(signed int j = 7; j > -1; j--){
      if(get_piece(Position(j, i)) != nullptr){
	Piece* temp = get_piece(Position(j, i));

	if((*temp).owner() == player){
	  enemy_pieces.push_back(Position(j, i));
	}
	   
	if((*temp).owner() != player && (*temp).piece_type() == 5){
	  king = Position(j, i);
	}
	
      }
    }
  }

  //Loop through and see if any pieces/positions in enemy piece position vector can reach King
  for(coordinates_loop it = enemy_pieces.begin(); it != enemy_pieces.end(); it++){
    Piece* holder = get_piece(*it);
    coordinates check_trajectory;
    if((*holder).valid_move_shape((*it), king, check_trajectory) > -1 && blockage(check_trajectory) == 1){
      return 1; //A piece can reach the king
    }
  }
  return 0; //None of the enemies' pieces can reach the king
}

//Check to see if a player has a possible move
//1 if a move is possible, 0 if no move is possible
int Game::checkmate(int player){

  //Populate a vector with all of owner's pieces
  //Populate a vector with the other spaces (enemy pieces / empty spaces)
  coordinates my_pieces;
  coordinates not_my_pieces;
  for(signed int i = 7; i > -1; i--){
    for(signed int j = 7; j > -1; j--){
      if(get_piece(Position(j, i)) != nullptr){
	Piece* temp = get_piece(Position(j, i));
	if((*temp).owner() == player){
	  my_pieces.push_back(Position(j, i));
	} else {
	  not_my_pieces.push_back(Position(j,i));
	} 
      } else { //empty spaces, we dont want to call get_piece() on a nullptr
	not_my_pieces.push_back(Position(j,i));
      }
    }
  }

  //Loop through both vectors, testing whether or not a piece can make a valid move
  //If Player A makes a move that results in Player A being checked, that move is invalid
  for(coordinates_loop not_me = not_my_pieces.begin(); not_me != not_my_pieces.end(); not_me++){
    for(coordinates_loop me = my_pieces.begin(); me != my_pieces.end(); me++){
      if(move(*me, *not_me) == 1 || move(*me, *not_me) == 3){
	return SUCCESS; //A move is still possible
      }
    }
  }
  return 0; //No moves are possible
}

//Checks to see if a pawn has moved down
void Game::pawn_into_queen(){
  for(int i = 0; i < 8; i++){
    Position empty;

    //Find an empty spot on the board to initalize our queen
    for(int j = 0; j < 8; j++){
      if(get_piece(Position(i, j)) == nullptr){
	empty = Position (i, j);
	j = 8;
      }
    }	
   
    if(get_piece(Position(i , 0)) != nullptr){
      Piece* temp = get_piece(Position(i, 0));
      if(temp->piece_type() == 0){
	init_piece(4, BLACK, empty);
	capture(empty, Position(i, 0));
      }
    }

    if(get_piece(Position(i, 7)) != nullptr){
      Piece* temp = get_piece(Position(i, 7));
      if(temp->piece_type() == 0){
	init_piece(4, WHITE, empty);
	capture(empty, Position(i,7));
      }
    }
    
  }
}

// Search the factories to find a factory that can translate
// piece_type' into a Piece, and use it to create the Piece.
// Returns nullptr if factory not found.
Piece* Game::new_piece(int piece_type, Player owner) {

    PieceGenMap::iterator it = _registered_factories.find(piece_type);

    if (it == _registered_factories.end()) { // not found
        std::cout << "Piece type " << piece_type << " has no generator\n";
        return nullptr;
    } else {
        return it->second->new_piece(owner);
    }
}

// Add a factory to the Board to enable producing
// a certain type of piece. Returns whether factory
// was successfully added or not.
bool Game::add_factory(AbstractPieceFactory* piece_gen) {
    // Temporary piece to get the ID
    Piece* p = piece_gen->new_piece(WHITE);
    int piece_type = p->piece_type();
    delete p;

    PieceGenMap::iterator it = _registered_factories.find(piece_type);
    if (it == _registered_factories.end()) { // not found, so add it
        _registered_factories[piece_type] = piece_gen;
        return true;
    } else {
        std::cout << "Piece type " << piece_type << " already has a generator\n";
        return false;
    }
}

//Swaps the positions of two places on the board
void Game::swap_positions(Position start, Position end){
  Piece* temp = _pieces[index(end)];
  _pieces[index(end)] = _pieces[index(start)];
  _pieces[index(start)] = temp;
}

//Executes the capturing of a piece
void Game::capture(Position start, Position end){
  delete _pieces[index(end)];
  _pieces[index(end)] = _pieces[index(start)];
  _pieces[index(start)] = nullptr;
}


//Checks for "blockage" between two spaces
//Returns -5 if the move cannot be executed
//Returns 1 if move can be executed
int Game::blockage(coordinates& trajectory){
  if(trajectory.empty()){
    return SUCCESS;
  }
  for(coordinates_loop it = trajectory.begin(); it != trajectory.end(); it++){
    if(get_piece(*it) != nullptr){
      return MOVE_ERROR_BLOCKED;
    }
  }
  return SUCCESS;
}

//Function to check end position, potentially capture or fail
int Game::final_space_check(Position end, int piece_owner){
  if(get_piece(end) == nullptr){
    return SUCCESS;
  }
  Piece* end_piece = get_piece(end);
  int end_piece_owner = (*end_piece).owner();
  if(end_piece_owner == piece_owner || end_piece_owner == NO_ONE){
    return MOVE_ERROR_BLOCKED;
  } else {
    return MOVE_CAPTURE;
  }
}
  
//Takes in user input and reports is it specifies the position of a piece
//And whether or not the end position is a valid location
//Stores those positions in a Position type vector, starting position then ending position
//Returns 1 if input is "good" and -7 if input is "bad"
int Game::accept_input(coordinates& start_end, char* input){
  string pos_start, pos_end;
  std::stringstream is(input);
  is >> pos_start >> pos_end;

  if(pos_start.length() != 2 || pos_end.length() != 2){
    Prompts::parse_error();
    return MOVE_ERROR_ILLEGAL;
  }
  
  string letters = "AaBbCcDdEeFfGgHh";
  unsigned int pos_start_x = 10;
  unsigned int pos_end_x = 10;
  for(unsigned int i = 0; i < letters.length(); i ++){
    if(pos_start[0] == letters[i]){
      pos_start_x = i/2;
    }
    if(pos_end[0] == letters[i]){
      pos_end_x = i/2;
    }
  }
  if(pos_start_x == 10 || pos_end_x == 10){
    Prompts::out_of_bounds();
    return MOVE_ERROR_OUT_OF_BOUNDS;
  }
  
  string numbers = "12345678";
  unsigned int pos_start_y = 10;
  unsigned int pos_end_y = 10;
  for(unsigned int j = 0; j < numbers.length(); j++){
    if(pos_start[1] == numbers[j]){
      pos_start_y = j;
    }
    if(pos_end[1] == numbers[j]){
      pos_end_y = j;
    }
  }
  if(pos_start_y == 10 || pos_end_y == 10){
    Prompts::out_of_bounds();
    return MOVE_ERROR_OUT_OF_BOUNDS;
  }
  
  Position start = Position(pos_start_x, pos_start_y);
  Position end = Position(pos_end_x, pos_end_y);
  start_end.push_back(start);
  start_end.push_back(end);
  return SUCCESS;
}

//Function to load in a file with a saved game baord
void Game::load_file(std::string filename) {

        int owner;
        int type;
	int temp;
	int rand;

	std::ifstream inFile;
        inFile.open(filename);

        if(inFile.is_open()) {
                string line, gametype, token, type_str, owner_str, full_position;

		getline(inFile, line); //This line has the gametype
		std::stringstream iss(line);
		iss >> gametype;
		
		getline(inFile, line); //This line has the turn number
                std::stringstream is(line);
                is >> temp;
                this->_turn = temp;

		if(gametype == "spooky"){
			getline(inFile, line);
			std::stringstream iss(line);
			iss >> rand;
			this->_random = rand;
		}

                while(getline(inFile, line)){
			
			std::stringstream iss(line);
			iss >> owner_str >> full_position >> type_str;
                        if(owner_str == "0"){
                                owner = 0;}
                        else if(owner_str == "1"){
                                owner = 1;}
                        else{
                                owner = 2;}
			
                        string letters = "abcdefgh";
			int row = 5;
                        for(unsigned int i = 0; i < letters.length(); i++){
                                if(full_position[0] == letters[i]){
                                        row = i;
                                }
                        }

                        string numbers = "01234567";
			int col = 0;
                        for(unsigned int i = 0; i < numbers.length(); i ++){
                                if(full_position[1] == numbers[i]){
                                        col = i;
                                }
                        }

                        if(type_str == "0"){
                                type = 0;}
                        else if(type_str == "1"){
                                type = 1;}
                        else if(type_str == "2"){
                                type = 2;}
                        else if(type_str == "3"){
                                type = 3;}
                        else if(type_str == "4"){
                                type = 4;}
                        else if(type_str == "5"){
                                type = 5;}
			else if(type_str == "6"){
				type = 6;
			}

			if(owner == 0){
				init_piece(type, WHITE, Position(row, col));
			}
			else if(owner == 1){
				init_piece(type, BLACK, Position(row, col));
			}
			else if(owner == 2){
				init_piece(type, NO_ONE, Position(row, col));
			}
        	}
	}
        else{
                Prompts::load_failure();
  }
  inFile.close();

}

//Function to save the game board into a file
void Game::save_file(string saveGame) {

  ofstream outFile;
  outFile.open(saveGame);
  //checks if infile is open;                                                                                                                                                                               
  if(outFile.is_open()) {

    outFile << get_gametype() << endl;           

    outFile << turn() << endl;                                                                                                                    
    if(get_gametype() == "spooky"){
	    outFile << random() << endl;
    }                                                                                                                                                                             

    //loops through each position on the board.
    for (int i = 0; i < 64; i++){
	Position piecePosition = Position(i%8, i/8);
        if (get_piece(piecePosition) != nullptr) {
          Piece* currentPiece = get_piece(piecePosition);
          int yAxis = i/8;
	  char letter = i % 8 + 97;
	  outFile << currentPiece->owner() << " " << letter << yAxis << " " << currentPiece->piece_type() << endl;
        }
      }

  }

  else {
    Prompts::save_failure();
  }
  outFile.close();
}

//Print out the current state of the board
void Game::print_board(){
  cout << endl;
  for(signed int i = 7; i > -1; i--){
    cout << "  "; 
    non_piece_lines(i); 
    for(signed int j = 0; j < 8; j++){ 
      if(j == 0){
	 Terminal::set_default();
	 cout << i + 1 << " "; 
      } 
      if(get_piece(Position(j, i)) == nullptr){
	if((i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1)) { 
	  Terminal::color_bg(tile_color); 
	}
	else { 
	Terminal::color_bg(Terminal::Color::WHITE); 
	}
	cout << "     ";
        }
        else {
	Piece* temp = get_piece(Position(j, i));
	if((i % 2 == 0 && j % 2== 0) || (i % 2 == 1 && j % 2 == 1)) {
	  Terminal::color_all(piece_brightness, piece_color, tile_color); 
	}
	else {
	Terminal::color_all(piece_brightness, piece_color, Terminal::Color::WHITE); 
	}	
	finish_print(temp);
      }     

    }
    Terminal::set_default();
    cout << endl; 
    cout << "  "; 
    non_piece_lines(i);
  }
  cout << "  " <<  "  a  " << "  b  " << "  c  " << "  d  " << "  e  " << "  f  " << "  g  " << "  h  " << endl << endl;
}

//Helper function for print_board
void Game::finish_print(Piece* value){
  if((*value).owner() == 0){
    switch((*value).piece_type()){
    case 0:
      cout << "  \u2659  ";
      break;
    case 1:
      cout << "  \u2656  ";
      break;
    case 2:
      cout << "  \u2658  ";
      break;
    case 3:
      cout << "  \u2657  ";
      break;
    case 4:
      cout << "  \u2655  ";
      break;
    case 5:
      cout << "  \u2654  ";
      break;
    }
  } else {
    if((*value).owner() == 1){
      switch((*value).piece_type()){
      case 0:
	cout << "  \u265f  ";
	break;
      case 1:
	cout << "  \u265c  ";
	break;
      case 2:
	cout << "  \u265e  ";
	break;
      case 3:
	cout << "  \u265d  ";
	break;
      case 4:
	cout << "  \u265b  ";
	break;
      case 5:
	cout << "  \u265a  "; 
	break;
      }
    }
  }
  if((*value).owner() == 2){
    cout << "  \u203C  ";
  }
}
void Game::non_piece_lines(int column) {
  for (int i = 0; i < 8; i++) { 
    if ((column % 2 == 0 && i % 2 == 0) || (column % 2 == 1 && i % 2 == 1)) { 
    Terminal::color_bg(tile_color);
    }
    else {
      Terminal::color_bg(Terminal::Color::WHITE);
	}
    cout << "     "; 

  } 
  Terminal::set_default();
  cout << endl; 
}

void Game::color_changer(string input_string) { 

  int temp_brightness = -1; 
  int temp_color = -1; 
  int temp_tile_color = -1;
  string flush; 
  
  if(input_string.compare("color_fg") == 0 || input_string.compare("color_all") == 0) {
    Prompts::brightness(); 
    cin >> temp_brightness;
    while(temp_brightness != 0 && temp_brightness != 1) { 
      Prompts::brightness_error(); 
      cin >> temp_brightness;
    } 
  }
  if(input_string.compare("color_fg") == 0 || input_string.compare("color_all") == 0) {
    Prompts::foreground_color(); 
    cin >> temp_color;
    while(temp_color != 0 && temp_color != 1 && temp_color != 2 && temp_color != 3 && temp_color != 4 && temp_color != 5 && temp_color != 6 && temp_color != 7 && temp_color != 9) { 
      Prompts::color_error(); 
      cin >> temp_color;
    } 
  }
  if(input_string.compare("color_bg") == 0 || input_string.compare("color_all") == 0) {
    Prompts::background_color();
      cin >> temp_tile_color;
      while (temp_tile_color != 0 && temp_tile_color != 1 && temp_tile_color != 2 && temp_tile_color != 3 && temp_tile_color != 4 && temp_tile_color != 5 && temp_tile_color != 6 && temp_tile_color != 7 && temp_tile_color != 9) { 
      Prompts::color_error(); 
      cin >> temp_tile_color;
    } 
  }

  if (input_string.compare("color_fg") == 0 || input_string.compare("color_all") == 0) {
    piece_brightness = temp_brightness; 
    piece_color = static_cast<Terminal::Color>(temp_color); 
  }
  if (input_string.compare("color_bg") == 0 || input_string.compare("color_all") == 0) { 
    tile_color = static_cast<Terminal::Color>(temp_tile_color);
 
      }                                                                                            
}                                                                                               
    
