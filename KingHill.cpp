/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#include "Variants.h"

KingHill::KingHill() : ChessGame(){
  //Fill in Hill vector with central positions
  Hill.push_back(Position(3, 4)); 
  Hill.push_back(Position(4, 3)); 
  Hill.push_back(Position(4, 4));
  Hill.push_back(Position(3, 3)); 

}

KingHill::KingHill(const std::string filename) : ChessGame(filename) {
  //Fill in Hill vector with central positions
  Hill.push_back(Position(3, 4));
  Hill.push_back(Position(4, 3));
  Hill.push_back(Position(4, 4));
  Hill.push_back(Position(3, 3));
} 

int KingHill::end_turn(){

  //Check to see if current player's king has moved to the middle
  for(int i = 0; i < 4; i ++) { 
    if(get_piece(Hill[i]) != nullptr) { 
      Piece* temp = get_piece(Hill[i]);
      if(temp->piece_type() == 5) {
	Prompts::conquered(player_turn());
	Prompts::win(player_turn(), _turn); 
	return 1; 
      }
    }
  } //Same as normal chess
  if(in_check(player_turn())){
    _turn += 1;
    if(checkmate(player_turn()) == 0){
      _turn -= 1;
      Prompts::checkmate(player_turn());
      Prompts::win(player_turn(), _turn);
      return 1;
    } else {
      _turn -= 1;
      Prompts::check(player_turn());
    }
  } else {
    _turn += 1;
    if(checkmate(player_turn()) == 0){
      _turn -= 1;
      Prompts::stalemate();
      return 1;
    }
    _turn -= 1;
  }
  return 0;
  
}
