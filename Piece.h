/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#ifndef PIECE_H
#define PIECE_H

#include "Enumerations.h"
#include <vector>
#include <cmath>

// Forward declaration of Piece class, present here so classes above 
// the Piece class definition in this file can refer to Piece as a type.
class Piece;


// Piece type enumeration.
enum PieceEnum {
  PAWN_ENUM = 0,
  ROOK_ENUM,
  KNIGHT_ENUM,
  BISHOP_ENUM,
  QUEEN_ENUM,
  KING_ENUM,
  GHOST_ENUM  //ghost piece used in Spooky Chess only
};


// A (virtual) class responsible for creating new instances of a
// particular type of piece (factory pattern).
class AbstractPieceFactory {
public:
    // Create a piece with the specified owner
    virtual Piece* new_piece(Player owner) const = 0;
    virtual ~AbstractPieceFactory() {}
};


// A templated class generating Pieces.
template <class T>
class PieceFactory : public AbstractPieceFactory {

public:
    PieceFactory(int piece_type) : _piece_type(piece_type) {}
    Piece* new_piece(Player owner) const override {
        return new T(owner, _piece_type);
    }

protected:
    int _piece_type;
};



// Class representing an individual piece
class Piece {

public:

    // There is no public constructor: only use factories to build this
    // class's subclasses

    // Destructor
    virtual ~Piece() {}

    // Returns the owner of the piece.
    Player owner() const { return _owner; }

    // Returns the piece_type of the piece.
    int piece_type() const { return _piece_type; }

    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const = 0;

    
    // Returns an integer representing whether a non-straight-line move is correctly diagonal
    // where a value >= 0 means valid, < 0 means invalid.
    // (applies only to those pieces which can move diagonally)
    int valid_diagonal(Position start, int delta_x, int delta_y, std::vector<Position>& trajectory) const {

	    //absolute value of movement in x-direction
            int abs_delta_x = std::abs(delta_x);
            //absolute value of movement in y-direction
            int abs_delta_y = std::abs(delta_y);
	    
	    // If the piece does not have true diagonal movement
	    if(abs_delta_x != abs_delta_y){
		    return -11; //MOVE_ERROR_ILLEGAL;
	    }
	    
	    int quad;
	    Position step;
	    int distance = abs_delta_x; //or abs_delta_y, chosen arbitrarily this corresponds to the diagonal distance

	    // set index to be equal to the value of the quadrant were moving to (Cartesian plane assumed)
	    if(delta_x > 0 && delta_y > 0){
		    quad = 1;}
	    else if(delta_x < 0 && delta_y > 0){
		    quad = 2;}
	    else if(delta_x < 0 && delta_y < 0){
		    quad = 3;}
	    else if(delta_x > 0 && delta_y < 0){
		    quad = 4;}
	    
	    //for loop using index (quadrant being moved into) to add the correct diagonal trajectory
	    for(int i = 1; i < distance; i++){
		    if(quad == 1){
			    step = Position(start.x + i, start.y + i);
			    trajectory.push_back(step);
                    }
		    if(quad == 2){
			    step = Position(start.x - i, start.y + i);
			    trajectory.push_back(step);
		    }
		    if(quad == 3){
                            step = Position(start.x - i, start.y - i);
                            trajectory.push_back(step);
                    }
		    if(quad == 4){
                            step = Position(start.x + i, start.y - i);
                            trajectory.push_back(step);
                    }
	    }
            //else return 1 to indicate a successful diagonal move after adding the correct trajectory
	    return 1; //SUCCESS;
    }

    void set_piece_type(int type){
      _piece_type = type;
    }

protected:
    Player _owner;
    int _piece_type;

    // Constructs a piece with a specified owner
    // Note that this is deliberately made protected. Use the factory only!
    Piece(Player owner, int piece_type) : _owner(owner) , _piece_type(piece_type) {}
};


#endif // PIECE_H
