/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#include "Variants.h"
#include <random>
#include <ctime>

Spooky::Spooky() : ChessGame(){
   init_piece(GHOST_ENUM, NO_ONE, Position(0, 4));
   srand(322);
}

Spooky::Spooky(const std::string filename) : ChessGame(filename) {
  //call random number generator _rand number of times to put it in the same place as when the game was saved
  int yay;
  srand(322);
  for(int i = 0; i < random(); i++){
    yay = rand() % 8; // NEED to get rid of this warning somehow
  }
  yay += 2;
}

void Spooky::spooky_turn() {
   coordinates ghost;

   //Find position of Ghost
   for(signed int i = 7; i > -1; i --){
     for(signed int j = 0; j < 8; j++){
       if(get_piece(Position(j, i)) != nullptr){
	 Piece * find_ghost = get_piece(Position(j, i));
	 if(find_ghost->piece_type() == GHOST_ENUM){
	   ghost.push_back(Position(j, i));
	 }
       }
     }
   }
   //Generate end position of ghost
   generate_end(ghost);
   Position start = *(ghost.begin());
   Position end = ghost.back();

   //Capture or move
   if(get_piece(end) != nullptr){ //A piece is present, not a king though
     capture(start, end);
     Prompts::ghost_capture();
   } else { //Moving to an empty space
     swap_positions(start, end);
   }
}

void Spooky::generate_end(coordinates& end){
  int random_num = rand() % 64;
  _random++; //increase Game variable to keep track of random called
  int row = random_num % 8;
  int col = random_num / 8;
  Position check_for_king = Position(row, col);
  
  if(get_piece(check_for_king) != nullptr){
    Piece * check_king= get_piece(check_for_king);
    if(check_king->piece_type() == 5 || check_king->piece_type() == 6){//Check for not Ghost or King
      generate_end(end);
    } else {
      end.push_back(check_for_king); //return end position
    }
  } else {
    end.push_back(check_for_king); //return end position
  }
}
  
  
