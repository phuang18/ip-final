/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#ifndef CHESS_GAME_H
#define CHESS_GAME_H

#include "Game.h"
#include "ChessPiece.h"


class ChessGame : public Game {

public:
    // Creates new game in standard start-of-game state 
    ChessGame();

    // Creates game with state indicated in specified file
    ChessGame(std::string filename);

protected:
    // Create all needed factories for the kinds of pieces
    // used in chess (doesn't make the actual pieces)
    virtual void initialize_factories();

};

#endif // CHESS_GAME_H
