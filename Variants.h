/*
 * Final Project
 * Intermediate Programming Fall 2017: 601.220
 * John Graham Reynolds - jreyno37
 * Alexander Chatterton - achatte9
 * Peter Huang -phuang18
 */

#include "ChessGame.h"

class KingHill : public ChessGame { 
public: 
 KingHill(); 
 KingHill(std::string filename);
protected:
 //Overrides the end turn function from Game to add the new win condition
  int end_turn() override;

  //Stores the Positions of the Hill
  coordinates Hill; 
};

class Spooky : public ChessGame {
public:
 Spooky();
 Spooky(std::string filename);
protected:
 //Overrides to allow the Ghost piece to move in Spooky CHess
 void spooky_turn() override;

 //Generates a "good" position to move to, recursive call until it finds one
 void generate_end(coordinates& end);
};
