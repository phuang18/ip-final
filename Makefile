CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

play: Play.o Game.o ChessGame.o ChessPiece.o KingHill.o Spooky.o
	$(CXX) Play.o Game.o ChessGame.o ChessPiece.o KingHill.o Spooky.o -o play

Play.o: Play.cpp Game.h ChessGame.h Prompts.h
	$(CXX) $(CXXFLAGS) -c Play.cpp

Game.o: Game.cpp Game.h Piece.h Prompts.h Enumerations.h Terminal.h
	$(CXX) $(CXXFLAGS) -c Game.cpp

ChessGame.o: ChessGame.cpp Game.h ChessGame.h Piece.h ChessPiece.h Prompts.h Enumerations.h
	$(CXX) $(CXXFLAGS) -c ChessGame.cpp

ChessPiece.o: ChessPiece.cpp Game.h Piece.h Enumerations.h Prompts.h
	$(CXX) $(CXXFLAGS) -c ChessPiece.cpp

KingHill.o: KingHill.cpp Variants.h Game.h ChessGame.h Piece.h ChessPiece.h Prompts.h Enumerations.h
	$(CXX) $(CXXFLAGS) -c KingHill.cpp

Spooky.o: Spooky.cpp Variants.h Game.h ChessGame.h Piece.h ChessPiece.h Prompts.h Enumerations.h
	$(CXX) $(CXXFLAGS) -c Spooky.cpp

clean:
	rm *.o play

